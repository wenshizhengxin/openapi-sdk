<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021-07-10
 * Time: 12:37
 */

namespace wenshizhengxin\openapi_sdk;


use epii\api\result\ApiResult;
use epii\http\http;
use epii\sign\sign;

class OpenApi
{
    private static $apiurl = "http://open.api.wszx.cc/server.php?server=";
    private static $key;
    public static function init($key){
        self::$key = $key;
    }
    public static function callService($serviceName,$data){
        sign::encode($data, self::$key);
        return new ApiResult(http::post(self::$apiurl.$serviceName,$data));
    }
}